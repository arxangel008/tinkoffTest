package com.tinkoff.service;

import com.tinkoff.dto.RequestDto;

public interface TranslateService {

    String getTranslate(RequestDto requestDto);
}
